import React from "react";
import { BrowserRouter, Switch, Route} from 'react-router-dom';
import {FormPost, Post} from "../pages";

const Routers = () => {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path={'/'} component={Post}></Route>
          <Route exact path={'/post/form'} component={FormPost}></Route>
          <Route exact path={'/post/form/:Id'} component={FormPost}></Route>
        </Switch>
      </BrowserRouter>
    );
}

export default Routers;