import React, {useEffect, useState} from "react";
import { useHistory, useParams } from 'react-router-dom';
import PostIdb from "../../data/postsIdb";

const FormPost = () => {
  const { Id } = useParams();
  const history = useHistory();
  const [input, setInput] = useState({
    title: '',
    image: '',
    body: '',
  });

  useEffect(() => {
    if (Id !== undefined) {
      PostIdb.getPostById(Id)
        .then((res) => {
          const data = res.data;
          setInput({
            title: data.title,
            image: data.image,
            body: data.body,
          });
        })
        .catch((err) => console.error(err.message));;
    }
  },[Id])

  const handleOnChange = (e) => {
      const {name, value} = e.target;
      setInput({...input, [name]:value})
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const {title, image, body} = input;

    if (Id !== undefined){
      PostIdb.updatePost(Id, title, image, body).then(() => {
        history.push('/');
        alert("Anda Berhasil Update Data");
      }).catch((err) => console.error(err.message))
    }else{
      PostIdb.createPost(title, image, body).then(() => {
        history.push('/');
        alert("Anda Berhasil Menambahkan Data");
      }).catch((err) => console.error(err.message));
    }
  }

  return (
    <React.Fragment>
      <div
        className='card'
        style={{ width: '1200px', margin: 'auto', marginTop: '40px' }}
      >
        <div className='card-header'>
          <h1 style={{ fontSize: '28px', textAlign: 'center' }}>Form Post</h1>
        </div>
        <form style={{ padding: '20px 40px' }} onSubmit={handleSubmit}>
          <div className='mb-3'>
            <label className='form-label' style={{fontWeight:'600', fontSize:'16px'}}>Title</label>
            <input type='title' name="title" className='form-control' onChange={handleOnChange} value={input.title} />
          </div>
          <div className='mb-3'>
            <label className='form-label' style={{fontWeight:'600', fontSize:'16px'}}>Url Gambar</label>
            <input type='title' name="image" className='form-control' onChange={handleOnChange} value={input.image} />
          </div>
          <div className='mb-3'>
            <label className='form-label' style={{fontWeight:'600', fontSize:'16px'}}>Post</label>
            <textarea name="body" className='form-control' rows={'8'} onChange={handleOnChange} value={input.body}></textarea>
          </div>
          <button style={{display:'block', width:'100%'}} type='submit' className='btn btn-primary'>
            Save
          </button>
        </form>
      </div>
    </React.Fragment>
  );
}

export default FormPost;