import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import PostIdb from "../../data/postsIdb";

const Post = () => {
  const history = useHistory()
  const [posts, setPosts] = useState([]);
  const [fetchStatus, setFetchStatus] = useState(true);

  useEffect(() => {
    if(fetchStatus){
      getData();
      setFetchStatus(false);
    }
  }, [fetchStatus, setFetchStatus]);

  const handleEdit = (e) => {
    const idPost = parseInt(e.target.value);
    
    history.push(`/post/form/${idPost}`);
  }

  const handleDelete = (e) => {
    const { value } = e.target;
    PostIdb.deletePost(value).then((res) => {
      getData();
    }).catch((err) => console.error(err.message));
  }

  const getData =  () => {
    PostIdb.getPost().then((res) => {
      const data = res.data;
      setPosts([...data]);
    })
    .catch((err) => {
      console.error(`masalah: ${err.message}`);
    });
  };

  return (
    <React.Fragment>
      <Link to={'post/form'}>
        <button className='btn btn-primary' style={{margin:'20px auto', display:'block'}}>Tambah Data</button>
      </Link>
      <table
        className='table table-hover'
        style={{ width: '1200px', margin: 'auto' }}
      >
        <thead>
          <tr style={{ textAlign: 'center', textTransform:'capitalize' }}>
            <th>No</th>
            <th>Gambar</th>
            <th>title</th>
            <th>post</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {posts !== null && (
            <React.Fragment>
              {posts.map((post, index) => {
                return (
                  <tr key={post.id} style={{textAlign:'center'}}>
                    <td>{index + 1}</td>
                    <td>
                      <img
                        src={post.image}
                        alt='gambar'
                        width={'200'}
                        height={'200'}
                      />
                    </td>
                    <td>{post.title}</td>
                    <td>{post.body}</td>
                    <td>
                      <div style={{ display: 'flex', justifyContent:'center' }}>
                        <button
                          className={'btn btn-info'}
                          style={{ marginRight: '10px', color:'#fff' }}
                          value={post.id}
                          onClick={handleEdit}
                        >
                          Edit
                        </button>
                        <button
                          className='btn btn-danger'
                          onClick={handleDelete}
                          value={post.id}
                        >
                          Delete
                        </button>
                      </div>
                    </td>
                  </tr>
                );
              })}
            </React.Fragment>
          )}
        </tbody>
      </table>
    </React.Fragment>
  );
}

export default Post;