import axios from "axios"
import API_ENDPOINT from "../globals/api-endpoint"

const PostIdb = {
    getPost : async () => {
        return axios.get(API_ENDPOINT.post);
    },
    getPostById : async (id) => {
        return axios.get(`${API_ENDPOINT.postById(id)}`);
    },
    deletePost: async (id) => {
        return axios.delete(`${API_ENDPOINT.postById(id)}`);
    },
    createPost : async (title, image, body) => {
        return axios.post(`${API_ENDPOINT.post}`, {title, image, body});
    },
    updatePost : async (id, title, image, body) => {
        return axios.put(`${API_ENDPOINT.postById(id)}`, { title, image, body });
    }
}

export default PostIdb;