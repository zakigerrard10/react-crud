import CONFIG from "./config"

const API_ENDPOINT = {
  post: `${CONFIG.BASE_URL}/posts`,
  postById : (id) => `${CONFIG.BASE_URL}/posts/${id}`,
};

export default API_ENDPOINT